---

- name: assert preconditions
  assert:
    that:
      - ansible_distribution == 'Ubuntu'
  tags: check-precond

- name: install prerequisites
  package:
    name: "{{ item }}"
    state: present
  with_items:
    - apt-transport-https
    - ca-certificates
    - curl

- name: add GPG key for Docker repository
  apt_key:
    id: 0EBFCD88
    url: https://download.docker.com/linux/ubuntu/gpg

- name: verify GPG key fingerprint
  shell: apt-key fingerprint 0EBFCD88 | grep "9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88"
  changed_when: false

- name: set repo channel for Ubuntu 17.04 Zesty
  set_fact:
    install_docker_channel_name: edge
  when: ansible_distribution == 'Ubuntu' and ansible_distribution_version == '17.04'

- name: set default repo channel
  set_fact:
    install_docker_channel_name: "{{ install_docker_channel_name | default('stable') }}"

- name: add Docker repository
  apt_repository:
    repo: deb [arch=amd64] https://download.docker.com/linux/ubuntu {{ ansible_distribution_release }} {{ install_docker_channel_name }}
    filename: docker
    state: present

- name: install Docker and support packages
  package:
    name: "{{ item }}"
    state: present
  with_items:
    - docker-ce
    - python-docker # for Ansible's Docker tasks
